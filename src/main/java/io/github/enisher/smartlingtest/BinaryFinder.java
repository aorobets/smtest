package io.github.enisher.smartlingtest;

/**
 * The class that contains methods that implements binary search algorithm.
 * 
 * Usage: <code>
 *   binaryFinder.indexOf(3, 1, 2, 3, 4, 5)
 * </code>
 * 
 * @author <a href="mailto:enisher@gmail.com">Artem Orobets</a>
 */
public class BinaryFinder {
  /**
   * Searches for specified value in specified array of integer values using the binary search algorithm.
   * 
   * The array must be sorted in descending order.
   * 
   * If the array contains multiple elements with the specified value, the the lowest index will be returned.
   * 
   * @param value
   *          to find
   * @param array
   *          the array to be searched
   * @return index of the value in the array or -1 if array does not contain specified value
   */
 public int indexOf(int value, int... array) {
    int from = 0;
    int to = array.length - 1;

    while (from <= to) {
      final int mid = (from + to) >>> 1;
      final int midValue = array[mid];

      if (value == midValue) {
        return indexOfMostLeftMatch(value, mid, array);
    } else if (value < midValue) {
        from = mid + 1;
      } else {
        to = mid - 1;
      }
    }

    return -1;
  }

  private int indexOfMostLeftMatch(int value, int result, int[] array) {
    while (result > 0 && array[result - 1] == value)
      --result;
    return result;
  }
}
