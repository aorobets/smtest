package io.github.enisher.smartlingtest;

import java.util.Iterator;

/**
 * @author <a href="mailto:enisher@gmail.com">Artem Orobets</a>
 */
public class SingleLinkedList<T> implements Iterable<T> {

  private ListItem<T> head;

  private SingleLinkedList(ListItem<T> head) {
    this.head = head;
  }

  public static <T> SingleLinkedList<T> create(T... values) {
    if (values.length == 0)
      return new SingleLinkedList<T>(null);
    else {
      ListItem<T> prev = null;
      for (int i = values.length - 1; i >= 0; i--) {
        T value = values[i];
        prev = new ListItem<T>(value, prev);
      }
      return new SingleLinkedList<T>(prev);
    }
  }

  @Override
  public Iterator<T> iterator() {
    return new ListIterator();
  }

  public void reverse() {
    ListItem<T> curr = head;
    ListItem<T> prev = null;
    while (curr != null) {
      final ListItem<T> next = curr.next;
      curr.next = prev;
      prev = curr;
      curr = next;
    }

    head = prev;
  }

  private static class ListItem<T> {
    private final T     value;
    private ListItem<T> next;

    private ListItem(T value) {
      this.value = value;
    }

    private ListItem(T value, ListItem<T> next) {
      this.value = value;
      this.next = next;
    }

    public T getValue() {
      return value;
    }

    public ListItem<T> getNext() {
      return next;
    }

    public void setNext(ListItem<T> next) {
      this.next = next;
    }
  }

  private class ListIterator implements Iterator<T> {
    private ListItem<T> nextItem = head;

    @Override
    public boolean hasNext() {
      return nextItem != null;
    }

    @Override
    public T next() {
      final T result = nextItem.value;
      nextItem = nextItem.next;
      return result;
    }
  }
}
