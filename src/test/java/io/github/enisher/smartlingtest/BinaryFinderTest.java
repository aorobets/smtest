package io.github.enisher.smartlingtest;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Test for {@link BinaryFinder}.
 */
@RunWith(Parameterized.class)
public class BinaryFinderTest {

  @Parameterized.Parameter(0)
  public String  testName;
  @Parameterized.Parameter(1)
  public int[]   array;
  @Parameterized.Parameter(2)
  public int     value;
  @Parameterized.Parameter(3)
  public int     expectedResult;

  private BinaryFinder binaryFinder;

  @Parameterized.Parameters(name = "{0}")
  public static Collection<Object[]> data() {
    final List<Object[]> data = new ArrayList<Object[]>();

    data.add(dataForFindFirst());
    data.add(dataForFindLast());
    data.add(dataForNotExistent());
    data.add(dataForEmptyArray());
    data.add(dataWithDuplications());
    data.add(dataWithDuplicationsInBeginning());

    return data;
  }

  private static Object[] dataForFindFirst() {
    return new Object[] { "dataForFindFirst", new int[] { 4, 3, 2, 1 }, 4, 0 };
  }

  private static Object[] dataForFindLast() {
    return new Object[] { "dataForFindLast", new int[] { 4, 3, 2, 1 }, 1, 3 };
  }

  private static Object[] dataForNotExistent() {
    return new Object[] { "dataForNotExistent", new int[] { 4, 3, 2, 1 }, 5, -1 };
  }

  private static Object[] dataForEmptyArray() {
    return new Object[] { "dataForEmptyArray", new int[] {}, 5, -1 };
  }

  private static Object[] dataWithDuplications() {
    return new Object[] { "dataWithDuplications", new int[] { 4, 3, 3, 3, 3, 3, 1 }, 3, 1 };
  }

  private static Object[] dataWithDuplicationsInBeginning() {
    return new Object[] { "dataWithDuplicationsInBeginning", new int[] { 3, 3, 3, 3, 3, 1 }, 3, 0 };
  }

  @Before
  public void setUp() throws Exception {
    binaryFinder = new BinaryFinder();
  }

  @Test
  public void testIndexOf() throws Exception {
    final int result = binaryFinder.indexOf(value, array);

    assertEquals(expectedResult, result);
  }
}
