package io.github.enisher.smartlingtest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class SingleLinkedListReverseTest {
  @Parameterized.Parameter(0)
  public String                     testName;
  @Parameterized.Parameter(1)
  public Integer[]                  values;
  @Parameterized.Parameter(2)
  public Integer[]                  expectedResult;

  private SingleLinkedList<Integer> list;

  @Parameterized.Parameters(name = "{0}")
  public static Collection<Object[]> data() {
    final List<Object[]> data = new ArrayList<Object[]>();

    data.add(new Object[] { "testReverse1Element", new Integer[] { 4 }, new Integer[] { 4 } });
    data.add(new Object[] { "testReverse4Elements", new Integer[] { 4, 3, 2, 1 }, new Integer[] { 1, 2, 3, 4 } });
    data.add(new Object[] { "testReverse0Elements", new Integer[] {}, new Integer[] {} });

    return data;
  }

  @Before
  public void setUp() throws Exception {
    list = SingleLinkedList.create(values);
  }

  @Test
  public void testReverse() throws Exception {
    list.reverse();

    SingleLinkedListHelper.assertListStructure(list, expectedResult);
  }
}
