package io.github.enisher.smartlingtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

/**
 * @author <a href="mailto:enisher@gmail.com">Artem Orobets</a>
 */
public class SingleLinkedListHelper {
  public static void assertListStructure(SingleLinkedList<Integer> list, Integer[] expectedResult) {
    final Iterator<Integer> iterator = list.iterator();
    for (Integer value : expectedResult) {
      assertTrue(iterator.hasNext());
      assertEquals(value, iterator.next());
    }
  }
}
