package io.github.enisher.smartlingtest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Test for {@link SingleLinkedList}.
 */
@RunWith(Parameterized.class)
public class SingleLinkedListCreateTest {

  @Parameterized.Parameter(0)
  public String    testName;
  @Parameterized.Parameter(1)
  public Integer[] values;

  @Parameterized.Parameters(name = "{0}")
  public static Collection<Object[]> data() {
    final List<Object[]> data = new ArrayList<Object[]>();

    data.add(new Object[] { "testAdd1Element", new Integer[] { 4 } });
    data.add(new Object[] { "testAdd4Elements", new Integer[] { 4, 3, 2, 1 } });
    data.add(new Object[] { "testAdd0Elements", new Integer[] {} });

    return data;
  }

  @Test
  public void testCreate() throws Exception {
    final SingleLinkedList<Integer> list = SingleLinkedList.create(values);

    SingleLinkedListHelper.assertListStructure(list, values);
  }

}
